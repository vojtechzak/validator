#!/usr/bin/env python3

import os, discord
from dotenv import load_dotenv
import requests
import json


load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
SYMBOLS = ['\u2796', '\u2705', '\u274C']

TASKS = json.load(open('tasks.json', 'r'))
USERS = json.load(open('users.json', 'r'))
RESULTS = json.load(open('results.json', 'r'))

def update_results():
	json.dump(RESULTS, open('results.json', 'w'), indent=4)

def add_user(id: str, name: str):
	USERS[id] = name
	json.dump(USERS, open('users.json', 'w'), indent=4)

async def send_embed(title: str, description: str, channel: discord.DMChannel):
	embed = discord.Embed(title=title, description=description)
	await channel.send(embed=embed)


async def evaluate_solution(attachments: 'list[discord.Attachment]', message: discord.Message):

	channel = message.channel
	user_id = str(message.author.id)
	add_user(user_id, message.author.name)

	for attachment in attachments:

		if not attachment.filename.endswith('.out'):
			return await send_embed('Error', 'File must end with `.out`', channel)

		name, ext = attachment.filename.split('.')
		task, num = name.split('-')

		if task not in TASKS:
			return await send_embed('Error', f'No task named `{task}`', channel)

		if num not in TASKS[task]:
			return await send_embed('Error', f'No input with number `{num}`', channel)

		try:
			with open(f'tasks/{task}/{task}-{num}.out', 'rb') as f:
				solution = f.read().replace(b'\r\n', b'\n').decode('utf-8').strip()
				submission = requests.get(attachment.url).content.replace(b'\r\n', b'\n').decode('utf-8').strip()

				correct = solution == submission

				if user_id not in RESULTS[task]:
					RESULTS[task][user_id] = {file: 0 for file in TASKS[task]}

				if RESULTS[task][user_id][num] != 1:
					RESULTS[task][user_id][num] = 1 if correct else 2

				update_results()

				await send_embed('Result', f'{task}-{num} is correct' if correct else f'{task}-{num} is wrong', channel)

		except Exception as e:
			await send_embed('Error', f'Exception: {e}', channel)


async def display_task_stats(task: str, channel: discord.DMChannel):

	if task not in TASKS:
		return await send_embed('Error', f'No task named `{task}`', channel)
	
	lines = []
	for id in RESULTS[task]:
		lines.append(f'**{USERS[id]}:** ' + ', '.join([f'{num} {SYMBOLS[RESULTS[task][id][num]]}' for num in TASKS[task]]))
	text = '\n'.join(lines)

	embed = discord.Embed(title=f'{task} overview', description=text)
	await channel.send(embed=embed)


async def display_personal_stats(id: str, channel: discord.DMChannel):

	tasks = [task for task in TASKS if str(id) in RESULTS[task]]

	if not tasks:
		await send_embed('Error', 'You have not submitted any solutions yet', channel)
		return

	lines = []
	for task in tasks:
		lines.append(f'**{task}:** ' + ', '.join([f'{num} {SYMBOLS[RESULTS[task][id][num]]}' for num in TASKS[task]]))
	text = '\n'.join(lines)

	embed = discord.Embed(title='Your stats', description=text)
	await channel.send(embed=embed)


async def display_task(task: str, channel: discord.TextChannel):

	if task not in TASKS:
		await send_embed('Error', f'No task named `{task}`', channel)
		return

	with open(f'tasks/{task}/{task}.json') as f:
		data = json.load(f)

		text = f'''
{data['description']}

*Tvar vstupu:* {data['informat']}

*Tvar výstupu:* {data['outformat']}

*Ukázkový vstup:*
{data['inexample']}
*Ukázkový výstup:*
{data['outexample']}
Výstupní soubory mi zasílejte s názvem `{task}-0x.out`

*Vstupní soubory:*'''

		files = [f'tasks/{task}/{task}-{file}.in' for file in TASKS[task]]
		embed = discord.Embed(title=data['title'], description=text)

		await channel.send(embed=embed)
		for file in files:
			await channel.send(file=discord.File(file))


async def handle_guild_message(message: discord.Message):

	if not message.author.id == 655369541228167170:
		return

	if not message.content.startswith('display '):
		return
	
	task = message.content.split(' ')[1]
	await display_task(task, message.channel)


async def handle_dm(message: discord.Message):

	user_id = str(message.author.id)
	admin = user_id == '655369541228167170'

	if message.content == 'stats':
		await display_personal_stats(user_id, message.channel)

	elif message.attachments:
		await evaluate_solution(message.attachments, message)

	elif message.content == 'tasks' and admin:
		for task in TASKS:
			await display_task_stats(task, message.channel)

	elif message.content.startswith('display '):
		task = message.content.split(' ')[1]
		await display_task(task, message.channel)

	else:
		await send_embed('Help', 'You can either send me a file or type `stats`', message.channel)


if __name__ == '__main__':

	intents = discord.Intents(messages=True, guilds=True)
	client = discord.Client(intents=intents)

	@client.event
	async def on_message(message: discord.Message):

		if message.author.bot:
			return

		if not message.guild:
			await handle_dm(message)

		else:
			await handle_guild_message(message)

	client.run(TOKEN)
